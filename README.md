# LandingPageCivitas

Repositório para a Landing Page do Civitas

1.  [Tecnologias](#tecnologias)
2.  [Funcionalidades](#funcionalidades)
3.  [O que aprendi](#o-que-aprendi)

### Tecnologias<br>

O site foi feito usando HTML5, CSS3 e JavaScript vanilla.

### Funcionalidades <br>

O site possui:
- aumento das imagens ao passar o mouse em cima
- carrossel animado que pausa ao descansar o mouse em cima

### O que aprendi

Fazendo esse site eu:

- aprendi a fazer um carrossel animado com JavaScript puro, usando setInterval()
- aprimorei meu aprendizado de flexbox
- melhorei minhas habilidades em HTML e CSS
- aprendi um pouco mais de Markdown para fazer esse ReadMe