const slides = document.getElementById("slides") //pega a div que estão os slides

const slide = document.querySelectorAll("#slides div") //pega todas as divs dentro do elemento com id #slides

let idSlide = 0;
let aux = 1;

function carrossel(){ //função pra passar de slide
    idSlide++;
    if(idSlide>slide.length-1){
        idSlide=0;
    }

    slides.style.transform = `translatex(${-idSlide*600}px)`; //avança pro proximo slide ou volta pro primeiro se estiver no ultimo
}
var intervalo = 1500; 
var refreshIntervalId = setInterval(carrossel, intervalo); //determina o tempo slide

function rodarCarrossel(){
    if(aux){ //evita que acumule vários setInterval e fique chamando mais de uma vez a função pra passar de slide
        refreshIntervalId = setInterval(carrossel, intervalo);
    }
    aux=0;
}

function pararCarrossel(){ //quando descansar o mouse sobre o slide vai parar de passar
    console.log("parar")
    aux=1;
    clearInterval(refreshIntervalId); //para o setInterval e para de rodar o slide
}


const nome = document.querySelector("#nome");
const email = document.querySelector("#email")
const erroNome = document.querySelector("#erro-nome");
const erroEmail = document.querySelector("#erro-email");
const sucesso = document.querySelector("#sucesso");

function validar(){
    console.log("validar");
    if(nome.value==""){
        erroEmail.style.display="none";
        erroNome.style.display="inline";
        erroNome.style.marginLeft="-300px";
    }
    else if(email.value==""){
        erroEmail.style.marginLeft="100px";
        erroNome.style.display="none";
        erroEmail.style.display="block";
    }
    else{
        erroNome.style.display="none";
        erroEmail.style.display="none";
        sucesso.style.display = "block";
        sucesso.style.marginLeft="-100px"
    }
}